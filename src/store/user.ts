import { defineStore } from 'pinia'
import router from "@/router";

export interface UserData {
  firstName: string,
  lastName: string,
  email: string,
  phone: string,
  birthday: string | Date,
  about: string,
  avatarUrl?: string,
}

export interface SnackbarInterface {
  visible: boolean,
  type: Snackbar,
  content: string
}

export enum Snackbar {
  success = 'success',
  danger = 'danger'
}

export const useUserStore = defineStore('user', {
  state: () => ({
    snackbar: <SnackbarInterface>{
      visible: false,
      type: Snackbar.success,
      content: ''
    },
    profile: <UserData>{
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      birthday: "",
      about: "",
      avatarUrl: ''
    }
  }),
  getters: {
    getProfile: state => state.profile,
    getSnackbarVisible: state => state.snackbar.visible
  },
  actions: {
    async saveUser(data: UserData) {
      this.profile = { ...data }
      this.showSnackbar('Your data has been saved correctly', Snackbar.success)
      await router.push('/user')
    },
    showSnackbar(content: string, type: Snackbar) {
      this.snackbar.type = type
      this.snackbar.content = content
      this.snackbar.visible = true
      setTimeout(() => {
        this.snackbar.content = ''
        this.snackbar.visible = false
      }, 3 * 1000)

    }
  },
  persist: {
    enabled: true,
  },
})
