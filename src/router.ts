import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import { Snackbar, useUserStore } from '@/store/user'

async function loadComponent(promise: Promise<unknown>) {
  try {
    return await promise;
  } catch (e) {
    await router.replace('/error')
  }
}

export const routes: Array<RouteRecordRaw> = [
  // error page is not designed
  {
    path: '/error', component: () => loadComponent(import('@/pages/Error.vue')), name: "Error",
  },
  {
    path: '/', component: () => loadComponent(import('@/pages/Default.vue')), name: "Default", redirect: '/user',
    children: [
      {
        path: '/user', component: () => loadComponent(import('@/pages/User.vue')), name: "User",
        // I assume that the completed form is an authorization form
        meta: {
          auth: true
        },
      },
      {
        path: '/edit',
        component: () => loadComponent(import('@/pages/Edit.vue')),
        name: "Edit",

      },
    ]
  },
]

const router = createRouter({
  history: createWebHistory('/'),
  routes,
})

router.beforeEach(async (to, prev) => {
  const user = useUserStore()
  if (to && to.matched && to.matched.length > 0) {
    if (to.meta.auth) {
      const profile = user.getProfile
      if (!profile.avatarUrl) {
        delete profile.avatarUrl
      }
      const isEmpty = !Object.values(profile).some((value) => value !== '');
      if (isEmpty) {
        if (prev.name === 'Edit') {
          user.showSnackbar('Complete the form first.', Snackbar.danger)
        }
          await router.replace('/edit')
      }
    }
  } else  {
    await router.push('/error')
  }

});

export default router;
