import { reactive, ref, watch, UnwrapRef } from "vue";

interface Errors {
  [key: string]: string | null | false;
}

export function useForm<T extends Record<string, unknown>>(initialData: T) {
  type SubmitCallback = ((data: T) => Promise<void>) | null;
  type Validator = (value: unknown, form: UnwrapRef<T> | T) => string | null | false;

  interface Validators {
    [key: string]: Validator | Validator[];
  }

  let submitCallback: SubmitCallback = null;
  let validators: Validators = {};

  const sending = ref(false);
  const errors = reactive<Errors>({});
  const form = reactive<T>(Object.assign({}, initialData));
  const submit = async () => {
    let hasErrors = false;
    sending.value = false;
    const data = Object.assign({}, form) as T;
    for (const field in form) {
      if (Object.prototype.hasOwnProperty.call(form, field)) {

        if (field in validators) {
          let result: string | false | null = null;
          const validatorsArray = !Array.isArray(validators[field]) ? [validators[field]] : validators[field];
          for (const validator of validatorsArray as Validator[]) {
            result = validator(data[field], form);
            if (result) {
              break;
            }
          }
          errors[field] = result;
          if (result) {
            hasErrors = true;
          }
        }
      }
    }
    if (!hasErrors && !sending.value && submitCallback) {
      sending.value = true;
      await submitCallback(data);
      sending.value = false;
    }
  };
  const onSubmit = (callback: SubmitCallback) => {
    submitCallback = callback;
  };
  const setValidators = (v: Validators) => {
    validators = v;
    for (const field in form) {
      if (Object.prototype.hasOwnProperty.call(form, field)) {
        if (field in validators) {
          watch(
            () => form[field],
            () => {
              errors[field] = null;
            },
          );
        }
      }
    }
  };


  return {
    form,
    errors,
    sending,
    submit,
    onSubmit,
    setValidators,
  };
}
