export default {
  required() {
    return (value: unknown) => (!value ? 'This field is required' : null);
  },
  email(value: unknown) {
    //pattern taken from https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
    const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !pattern.test(String(value).toLowerCase()) ? 'E-mail address is incorrect' : null;
  },
  minChars(min: number) {
    return (value: unknown) => (!value || String(value).length < min ? `Min ${min} chars` : null);
  },
  maxChars(max: number) {
    return (value: unknown) => (!value || String(value).length > max ? `Max ${max} chars` : null);
  },
  notANumber(value: unknown) {
    const pattern = /[^0-9 ]/g;
    return pattern.test(String(value)) ? 'Invalid number' : null;
  },
};
