import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPersist from 'pinia-plugin-persist'
import App from './App.vue'
import router from "./router";

// Stylesheets
import "@/assets/styles/tailwind.css";

const app = createApp(App);


async function init() {
  const pinia = createPinia()
  app.use(router);
  app.use(pinia)
  pinia.use(piniaPersist)
  app.mount('#app');
}

init()
