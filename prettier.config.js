module.exports = {
  printWidth: 120,
  proseWrap: "preserve",
  trailingComma: "all",
  singleQuote: false,
  useTabs: false,
  tabWidth: 2,
  arrowParens: "avoid",
  endOfLine: "auto",
  semi: false,
};
