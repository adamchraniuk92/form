

# User form

## Project setup

In the root folder

```
yarn
```

### Compiles and hot-reloads for development

```
yarn dev
```

### Compiles and minifies for production

```
yarn build
```

### Live demo

See [https://adam-user-form.netlify.app](https://adam-user-form.netlify.app).


### Summary

The project presents a sample user form that can be used in the administration panel.

The technologies used in the project:

- Vue 3
- Typescript
- Vite for development/build
- Vue-router
- Pinia for state management and persist data (an alternative to this could be the creation of a function for reading and writing data from session storage)
- Tailwind.css for styles (I wrote one of the components using sass to show an alternative solution)
- App is deployed on https://www.netlify.com/

### Why did I choose these technologies?
- I chose tailwind because it has all the necessary classes to create any layout which can also save a lot of time.
- Vue3 because I feel best in it.
- Typescript for better custom code documentation and avoiding unwanted bugs.
- Pinia for learning about new technology and manageability if the project grows.
- I also decided to write my own validators for the form.

### My assumptions
- Create a simple and friendly form
- Create reusable components as possible

### Why did I decide on these solutions and what can be improved?
I would love to talk with you about this
